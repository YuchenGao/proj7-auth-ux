import os
from flask import Flask, redirect, url_for, request, render_template,session
from pymongo import MongoClient
import flask
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging,password,testToken
from flask_restful import Resource, Api, reqparse, fields
from flask_login import LoginManager, UserMixin, current_user, login_user,logout_user, fresh_login_required, login_required
from flask_wtf.csrf import CsrfProtect
from flask_wtf import FlaskForm
###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = "24411224"
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
csrf = CsrfProtect()
class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active

    def is_active(self):
        return self.active

login_manager = LoginManager()
login_manager.setup_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)

@app.route("/login")
def login():
    app.logger.debug("Main page entry")
    return flask.render_template('login.html')
 
@app.route("/register")
def register():
    return flask.render_template('register.html') 

@app.route("/logout")
def logout():
    logout_user()
    flask.session.pop('token', None)
    return flask.render_template('calc.html') 
  
@app.route("/check_auth",methods=['POST'])
def check_auth():
    cur_user = request.form['username']
    cur_pass = request.form['password']
    cur_rem = request.form.get('rem')
    exists = False
    temp_pass = None

    _items = db.user_db.find()
    for item in _items:
        if cur_user == item['username']:
            exists = True
            temp_pass = item['password']
            break;

    if not exists:
        app.logger.debug("No User Name/Password")
        return render_template("login.html")

    if password.verify_password(cur_pass, temp_pass) :
            app.logger.debug("Matched Password!")
            t = testToken.generate_auth_token(600)
            if session.get('token'):
                session.pop('token')
            session['token'] = t
            return flask.render_template('logout.html')
    else:
        session['token'] = 'bad_token'

    return render_template("login.html"),401

@app.route("/new_user",methods=['POST'])
def new_user():
    cur_user = request.form['username']
    _items = db.user_db.find()
    for item in _items:
        if cur_user == item['username']:
            return flask.render_template('invalid.html'),400
    my_password = request.form['password']
    if my_password == "":
        return flask.render_template('invalid.html'),400

    encrypt_pass = password.hash_password(my_password)

    user_struct = {
        'username': request.form['username'],
        'password': encrypt_pass,
        'ip': None,
        'token': None
    }

    db.user_db.insert_one(user_struct)

    _items = db.user_db.find()
    for item in _items:
        app.logger.debug(item)

    return flask.render_template('login.html'),201

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.route("/submit", methods=['POST'])
def submit():
    data = request.form
    km = [];
    open_time = [];
    close_time= [];
    dist = 0;
    db.tododb.remove({})                           
    for key in data.keys():

       for value in data.getlist(key):             # For each value of each key
            if key == 'km' and value != '':       # Repeat for all other keys...
                km.append(value)
            elif key == 'open' and value != '':
                open_time.append(value)
            elif key == 'close' and value != '':
                close_time.append(value)
            elif key == 'distance' and value != '':
                dist=value
              
    item_doc = {"km": km, "open":open_time,"close": close_time,"dist": dist}
    db.tododb.insert_one(item_doc)

    return render_template('calc.html')
    
    
@app.route('/done')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('todo.html', items=items)


###
# Pages
###




@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))


    date = request.args.get('date',"",type=str)
    time = request.args.get('time',"",type=str)
    dist = request.args.get('dist',200,type=int)
    date_str = date + " " + time
    start_time = arrow.get(date_str).isoformat
    
    error_msg = ""
    if (km != 0):
        check = (km-dist)/km

    check = 0
    if km == 0:
        error_msg = "Error! Control distance is at ZERO!"
    else:
        check = (km-dist)/km
    if check > 0.2 :
        error_msg = "Warning! Control distance is 20% or longer than brever"
    
    times = date + " " + time
    open_time = acp_times.open_time(km, 200,times)
    close_time = acp_times.close_time(km, 200,times)
    result = {"open": open_time, "close": close_time,"error_msg":error_msg}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
