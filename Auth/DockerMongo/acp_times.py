"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#
MAX_TABLE = { 200: 34, 400:32,600:30,1000:28,1300:26 }
MIN_TABLE = { 200: 15, 400:15,600:15,1000:11.428,1300:13.333 }


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    value = 0
    last_dist = 0
    if(control_dist_km != 0):
        
        for dist in MAX_TABLE:
            if control_dist_km > dist:
                value += (dist-last_dist)/MAX_TABLE[dist]
                last_dist = dist
            else:
                value += (control_dist_km-last_dist)/MAX_TABLE[dist]
                break;
           
    hours = int(value)
    minutes = int((value*60)%60)
    seconds = int((value*3600)%60)
    date_time = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')                 
    date_time = date_time.shift(hours=+hours, minutes=+minutes)                    
    date_time = date_time.replace(tzinfo='US/Pacific')                             
    if seconds >= 30:                                                             
        date_time = date_time.shift(minutes=+1)                                    
    return date_time.isoformat()

                


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    value = 0
    last_dist = 0
 
    if(control_dist_km != 0):
        
        for dist in MIN_TABLE:
            if control_dist_km > dist:
                value += (dist-last_dist)/MIN_TABLE[dist]
                last_dist = dist
            else:
                value += (control_dist_km-last_dist)/MIN_TABLE[dist]
                break;
           
    hours = int(value)
    minutes = int((value*60)%60)
    seconds = int((value*3600)%60)
    date_time = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')                 
    date_time = date_time.shift(hours=+hours, minutes=+minutes)                    
    date_time = date_time.replace(tzinfo='US/Pacific')                             
    if seconds >= 30:                                                             
        date_time = date_time.shift(minutes=+1)                                    
    return date_time.isoformat()

